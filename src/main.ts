import { AuthService, LoginClass, LoginRequestDto, PoliciesService, TenantsService, UsersService, AccountsService, CreateAccountDto } from '@cover42/typescript-nestjs-sdk';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { CsvParser } from 'nest-csv-parser'
import { CsvService, CSVAccount } from './csv.service';
import { LOADIPHLPAPI } from 'dns';

class Account {
  id: number;
  accountCode: string;
  email: string;
}

function getGender(gender: string): CreateAccountDto.GenderEnum {
  if (gender === 'female') {
    return CreateAccountDto.GenderEnum.NUMBER_1
  }
  if (gender === 'male') {
    return CreateAccountDto.GenderEnum.NUMBER_0
  }
  return CreateAccountDto.GenderEnum.NUMBER_2
}

function getAccount(accounts: Account[], email: string): Account {
  for (var val of accounts) {
    if (val.email === email) {
      return val
    }
  }
  return null
}

async function createAccount(accountService: AccountsService, val: CSVAccount, accounts: Account[]) {

}


async function bootstrap() {
  const app = await NestFactory.createApplicationContext(AppModule);

  try {

    const authService = app.get(AuthService);
    const response = await authService.login({
      username: "demo+rv+admin@emil.de",
      password: "Emildemo2020",
      newPassword: "",
    });
    //console.log(response.data);

    authService.save("token", response.data.identityToken);


    const accountService = app.get(AccountsService);
    const csvService = app.get(CsvService)
    const entities = await csvService.parse("/home/sachin/works/sample/src/accounts.csv")


    const accListResp = await accountService.listAccounts(null, null, null, null, null, 5000)
    const accounts = accListResp.data as { accounts: Account[] }
    console.log(accounts.accounts)

    for (var a of accounts.accounts) {
      try {
        console.log("deleteing the account for the email address " + a.email + ", account Code " + a.accountCode);
        const del = await accountService.deleteAccount(a.accountCode);
        console.log("deleted account!")
      } catch (error) {
        console.log(error);
      }
    }

    for (var en of entities) {
      const val = en as CSVAccount
      console.log("==================== for account " + val.email + " ============================= ");
      try {
        console.log("creating account for " + val.email);
        const respAccCreate = await accountService.createAccount(
          {
            firstName: val.firstName,
            lastName: val.lastName,
            email: val.email,
            gender: getGender(val.gender),
            street: val.street,
            zipCode: val.zipCode,
            city: val.city,
            houseNumber: val.houseNumber,
            birthDate: val.birthDate,
            phone: val.phone,
            accountNumber: "" + val.id,
          }
        );
        console.log("created account for " + val.email);
        console.log(respAccCreate.data);
      } catch (error) {
        console.log(error);
      }
    }
    
  } catch (error) {
    console.log(error);
  }
  app.close();
}
bootstrap();