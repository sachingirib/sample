import { CacheModule, Module } from '@nestjs/common';
import { ApiModule, Configuration, ConfigurationParameters } from '@cover42/typescript-nestjs-sdk';
import { CsvModule } from 'nest-csv-parser'
import { CsvService } from './csv.service';

export function apiConfigFactory (): Configuration {
  const params: ConfigurationParameters = {
    basePath : "https://apiv2-perf.emil.de",
    accessTokenKey: "token"
  }
  return new Configuration(params);
}

@Module({
    imports: [ 
      CacheModule.register(), 
      ApiModule.forRoot(apiConfigFactory),
      CsvModule
    ],
    providers: [CsvService]

})
export class AppModule {}