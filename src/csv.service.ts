import { Injectable } from '@nestjs/common'
import { CsvParser } from 'nest-csv-parser'
import * as fs from 'fs';


//id,firstName,lastName,email,gender,street,houseNumber,zipCode,city,birthDate,phone
export class CSVAccount {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  gender: string;
  street: string;
  houseNumber: string;
  zipCode: string;
  city: string;
  birthDate: string;
  phone: string;
 }
@Injectable()
export class CsvService {
  constructor(
    private readonly csvParser: CsvParser
  ) {}
 
  async parse(path: string){
    const stream = fs.createReadStream(path);
    const entities = await this.csvParser.parse(stream, CSVAccount, null, null, { strict: true, separator: ',' })
    return entities.list
  }
}